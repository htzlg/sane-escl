# sane-escl
As part of a second year internship at EPITECH (School of IT and New Technologies),
I worked for four months in ORDISSIMO, a company that produces high-performance and
easy to use computers, especially for seniors and beginners in IT.

The Apple AirPrint specification 1.4 requires MFP (Multi-Function Printers; printers
with an attached scanner) to also allow scanning over the AirPrint connection.
The required protocol is eSCL, an XML over HTTP originally created by HP.
The "escl" backend for sane supports AirScan/eSCL devices that announce
themselves on mDNS as _uscan._utcp or _uscans._utcp.
If the device is available, the "escl" backend recovers these capacities.
The user configures and starts scanning.
Here is a possible list of supported devices: https://support.apple.com/en-us/HT201311

Thereby, my project during these four months was to develop a backend of a scanner
driver using the eSCL protocol, in Linux / C. This is intended to be able to use
Apple's AirScan feature.
The backend has been developed using SANE (Scanner Access Now Easy), an API that
provides standardized access to very many scanners. It’s used in the scan application
of ORDISSIMO because thanks to it, the applications can handle more than 70.000
different scanners, instead of a few hundred without.
The code below shows how to recover the connected eSCL devices (escl_devices.c),
the scanner's capabilities (escl_capabilities.c), his status (escl_status.c) ; then
how send to SANE the document to be scanned and recover it after the scan
(escl_newjob.c / escl_scan.c), by resetting the scanner after
each scan (escl_reset.c).
All these functions are called in the SANE functions (escl.c).
The backend uses the CURL and XML libraries, and it currently handles all JPEG
documents.

Successfully tested on :
```
BROTHER MFC-J890DW
CANON TS3150
CANON iR-ADV C3520i
EPSON ET-4750
EPSON ET-3750
EPSON XP-335
HP ENVY 4500
XEROX VERSALINK C7020
```

It should work but the AirScan feature is not implemented, so not successfully tested on :
```
BROTHER MFC-J4620DW
```

INSTALLATION
For debian systems :
Get sources :
```
git clone https://gitlab.com/sane-escl/sane-escl.git
```

Get development environment :
```
apt-get update
apt-get install libsane-dev libavahi-client-dev libavahi-core-dev libxml2-dev libcurl4-gnutls-dev libjpeg-dev
mv sane-escl sane-escl-0.1
rm -rf sane-escl-0.1/.git
mv sane-escl-0.1/debian .
tar cJvf sane-escl_0.1.orig.tar.xz sane-escl-0.1
mv debian sane-escl-0.1/
```

Build Sources :
```
cd sane-escl-0.1
debuild -us -uc
```

Install :
```
apt-get update
apt-get install ../sane-escl_0.1-1_amd64.deb
```


For redhat systems :
Get development environment :

```
yum install rpm-build autoreconf automake libtool libxml2-devel libcurl-devel libjpeg-devel sane-backends-devel avahi-devel libcurl-devel gnutls-devel libgcrypt-devel
```

Get sources :
```
git clone https://gitlab.com/sane-escl/sane-escl.git
rm -rf sane-escl/.git
mv sane-escl sane-escl-0.1
tar cJvf sane-escl_0.1.orig.tar.xz sane-escl-0.1
mkdir -p ~/rpmbuild/SOURCES/
mv sane-escl_0.1.orig.tar.xz ~/rpmbuild/SOURCES/
```


Build Sources :
```
rpmbuild -bp sane-escl-0.1/sane-escl.spec
rpmbuild -bc --short-circuit sane-escl-0.1/sane-escl.spec
rpmbuild -bi --short-circuit sane-escl-0.1/sane-escl.spec
rpmbuild -ba sane-escl-0.1/sane-escl.spec
```


Install :
```
rpm -i ~/rpmbuild/RPMS/x86_64/sane-escl-0.1-0.x86_64.rpm
```
