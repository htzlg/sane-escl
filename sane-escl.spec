%define VERSION 0.1
%define RELEASE 0

%define _arc  %(getconf LONG_BIT)
%define _is64 %(if [ `getconf LONG_BIT` = "64" ] ; then  printf "64";  fi)

%define _prefix /usr
%define _etcdir /etc
%define _sharedir %{_prefix}/share
%define _libdir %{_prefix}/lib%{_is64}

Summary: Sane eSCL Ver.%{VERSION} for Linux
Name: sane-escl
Version: %{VERSION}
Release: %{RELEASE}
License: See the LICENSE*.txt file.
Vendor: Ordissimo
Group: Applications/Graphics
Source0: sane-escl_%{version}.orig.tar.xz
BuildRoot: %{_tmppath}/%{name}-root
Requires: libcurl, gnutls, libgcrypt, sane-backends-libs, avahi-libs, libjpeg, libxml2
BuildRequires: libcurl-devel, libjpeg-devel, sane-backends-devel, avahi-devel, libcurl-devel, gnutls-devel, libgcrypt-devel, libxml2-devel


%description
eSCL backend for sane in Linux.
Backend sane implementing the eSCL protocol.


%prep
%setup -q -n sane-escl-%{version}


%build
#make


%install
# make install directory
#mkdir -p ${RPM_BUILD_ROOT}%{_libdir}/sane
mkdir -p ${RPM_BUILD_ROOT}%{_etcdir}/sane.d/dll.d/
mkdir -p ${RPM_BUILD_ROOT}%{_sharedir}/
#mkdir -p ${RPM_BUILD_ROOT}%{_datadir}/doc/sane-escl-%{version}

./autogen.sh --prefix=%{_prefix} --libdir=%{_libdir} --sysconfdir=%{_etcdir}
./configure --prefix=%{_prefix} --libdir=%{_libdir} --sysconfdir=%{_etcdir}
make
make install DESTDIR=${RPM_BUILD_ROOT}

%clean
rm -rf $RPM_BUILD_ROOT


%post
if [ -x /sbin/ldconfig ]; then
	/sbin/ldconfig
fi


%files
%defattr(-,root,root)
%{_libdir}/sane/*
%{_prefix}/share
%{_etcdir}/sane.d/dll.d/escl


%ChangeLog
